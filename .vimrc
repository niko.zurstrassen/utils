set ts=2 sw=2
set expandtab
colorscheme gruvbox
set bg=dark
set listchars=eol:¬,tab:->,trail:·,extends:>,precedes:<,space:•
set list
set nu
